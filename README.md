# play-with-torch-dev

Repository for playing the computer vision apps using PyTorch C++ API on Raspberry Pi using **NanoDet**. Tech stack: C++ & LibTorch. Python Version: <https://github.com/mheriyanto/play-with-torch>

## Build & Compile

```console
$ mkdir build && cd build
$ cmake -DCMAKE_PREFIX_PATH=/absolute/path/to/libtorch ..
$ make

$ ./NanoDet 1 ../models/nanodet.torchscript.pth ../samples/test.jpg

# Keterangan:
# For webcam mode=0, path is cam id; 
# For image demo, mode=1, path=xxx/xxx/*.jpg; 
# For video, mode=2; 
# For benchmark, mode=3 path=0.
```

## References

+ NanoDet: Super fast and lightweight anchor-free object detection model. [here](https://github.com/RangiLyu/nanodet)
