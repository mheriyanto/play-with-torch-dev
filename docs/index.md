# play-with-torch-dev
Repository for playing the computer vision apps: **Vehicle classification** on Raspberry Pi. 

## Tools
### Tested Hardware
+ RasberryPi 4 Model B [here](https://www.raspberrypi.org/products/raspberry-pi-4-model-b/), RAM: **4 GB** and Processor **4-core @ 1.5 GHz** 
+ microSD Card **64 GB**
+ 5M USB Retractable Clip 120 Degrees WebCam Web Wide-angle Camera Laptop U7 Mini or [Raspi Camera](https://www.raspberrypi.org/documentation/hardware/camera/)

### Tested Software
+ Ubuntu Desktop 20.10 [aarch64](https://ubuntu.com/download/raspberry-pi/thank-you?version=20.10&architecture=desktop-arm64+raspi ) **64 bit**, install on RasberriPi 4

### Install the prerequisites 

+ Install packages

```console
$ sudo apt install build-essential make cmake git python3-pip libatlas-base-dev
$ sudo apt install libssl-dev
$ sudo apt install libopenblas-dev libblas-dev m4 python3-yaml
$ sudo apt install libomp-dev
```

+ make swap space to 2048 MB

```console
$ free -h
$ sudo swapoff -a
$ sudo dd if=/dev/zero of=/swapfile bs=1M count=2048
$ sudo mkswap /swapfile
$ sudo swapon /swapfile
$ free -h
```


## Usage

```console
$ git clone https://gitlab.com/mheriyanto/play-with-torch-dev
$ cd play-with-torch-dev
$ mkdir build && cd build
$ cmake ..
$ make
$ ./play-with-torch-dev
```

## TO DO

- [ ] Implement Unit-Test: Test-Driven Development (TDD)


# Credit to
+ [Share PyTorch binaries built for Raspberry Pi](https://github.com/ljk53/pytorch-rpi)

